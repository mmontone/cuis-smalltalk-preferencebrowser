'From Cuis 4.2 of 25 July 2013 [latest update: #2739] on 29 July 2016 at 6:24:50.591938 pm'!
'Description Please enter a description for this package'!
!provides: 'ValueModels' 1 2!
!classDefinition: #ValueModel category: #ValueModels!
Object subclass: #ValueModel
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ValueModels'!
!classDefinition: 'ValueModel class' category: #ValueModels!
ValueModel class
	instanceVariableNames: ''!

!classDefinition: #AspectAdaptor category: #ValueModels!
ValueModel subclass: #AspectAdaptor
	instanceVariableNames: 'subject getter setter subjectSendsUpdates'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ValueModels'!
!classDefinition: 'AspectAdaptor class' category: #ValueModels!
AspectAdaptor class
	instanceVariableNames: ''!

!classDefinition: #PluggableAdaptor category: #ValueModels!
ValueModel subclass: #PluggableAdaptor
	instanceVariableNames: 'getBlock setBlock updateBlock'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ValueModels'!
!classDefinition: 'PluggableAdaptor class' category: #ValueModels!
PluggableAdaptor class
	instanceVariableNames: ''!

!classDefinition: #ValueHolder category: #ValueModels!
ValueModel subclass: #ValueHolder
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'ValueModels'!
!classDefinition: 'ValueHolder class' category: #ValueModels!
ValueHolder class
	instanceVariableNames: ''!


!ValueModel methodsFor: 'as yet unclassified' stamp: 'MM 7/21/2016 10:44'!
value
	^ self subclassResponsibility! !

!ValueModel methodsFor: 'as yet unclassified' stamp: 'MM 7/21/2016 10:44'!
value: anObject
	^ self subclassResponsibility ! !

!AspectAdaptor methodsFor: 'accessing' stamp: 'MM 7/17/2016 17:03'!
getter
	"Answer the value of getter"

	^ getter! !

!AspectAdaptor methodsFor: 'accessing' stamp: 'MM 7/17/2016 17:03'!
getter: anObject
	"Set the value of getter"

	getter _ anObject! !

!AspectAdaptor methodsFor: 'as yet unclassified' stamp: 'MM 7/29/2016 18:09'!
initialize: aSubject
	subject := aSubject.
	
	getter := #value.
	setter := #value:.
	subjectSendsUpdates := false.! !

!AspectAdaptor methodsFor: 'accessing' stamp: 'MM 7/17/2016 17:03'!
setter
	"Answer the value of setter"

	^ setter! !

!AspectAdaptor methodsFor: 'accessing' stamp: 'MM 7/17/2016 17:03'!
setter: anObject
	"Set the value of setter"

	setter _ anObject! !

!AspectAdaptor methodsFor: 'accessing' stamp: 'MM 7/17/2016 17:03'!
subject
	"Answer the value of subject"

	^ subject! !

!AspectAdaptor methodsFor: 'accessing' stamp: 'MM 7/17/2016 17:03'!
subject: anObject
	"Set the value of subject"

	subject _ anObject! !

!AspectAdaptor methodsFor: 'accessing' stamp: 'MM 7/29/2016 18:10'!
subjectSendsUpdates
	"Answer the value of subjectSendsUpdates"

	^ subjectSendsUpdates! !

!AspectAdaptor methodsFor: 'accessing' stamp: 'MM 7/29/2016 18:10'!
subjectSendsUpdates: anObject
	"Set the value of subjectSendsUpdates"

	subjectSendsUpdates _ anObject! !

!AspectAdaptor methodsFor: 'as yet unclassified' stamp: 'MM 7/17/2016 17:04'!
value
	^ subject perform: getter! !

!AspectAdaptor methodsFor: 'as yet unclassified' stamp: 'MM 7/29/2016 18:09'!
value: anObject
	
	subject perform: setter with: anObject.
	
	subjectSendsUpdates ifFalse: [
		self changed: anObject]! !

!AspectAdaptor class methodsFor: 'as yet unclassified' stamp: 'MM 7/17/2016 17:03'!
on: aSubject
	^ self new initialize: aSubject! !

!AspectAdaptor class methodsFor: 'as yet unclassified' stamp: 'MM 7/17/2016 17:07'!
on: anObject getter: aSymbol setter: otherSymbol
	^ (self on: anObject) 
			getter: aSymbol;
			setter: otherSymbol;
			yourself! !

!PluggableAdaptor methodsFor: 'accessing' stamp: 'MM 7/22/2016 16:01'!
getBlock
	"Answer the value of getBlock"

	^ getBlock! !

!PluggableAdaptor methodsFor: 'accessing' stamp: 'MM 7/22/2016 16:01'!
getBlock: anObject
	"Set the value of getBlock"

	getBlock _ anObject! !

!PluggableAdaptor methodsFor: 'as yet unclassified' stamp: 'MM 7/22/2016 16:20'!
initialize
	updateBlock := [:value | true]! !

!PluggableAdaptor methodsFor: 'accessing' stamp: 'MM 7/22/2016 16:01'!
setBlock
	"Answer the value of setBlock"

	^ setBlock! !

!PluggableAdaptor methodsFor: 'accessing' stamp: 'MM 7/22/2016 16:01'!
setBlock: anObject
	"Set the value of setBlock"

	setBlock _ anObject! !

!PluggableAdaptor methodsFor: 'accessing' stamp: 'MM 7/22/2016 16:01'!
updateBlock
	"Answer the value of updateBlock"

	^ updateBlock! !

!PluggableAdaptor methodsFor: 'accessing' stamp: 'MM 7/22/2016 16:01'!
updateBlock: anObject
	"Set the value of updateBlock"

	updateBlock _ anObject! !

!PluggableAdaptor methodsFor: 'as yet unclassified' stamp: 'MM 7/22/2016 16:03'!
value
	^ getBlock value! !

!PluggableAdaptor methodsFor: 'as yet unclassified' stamp: 'MM 7/22/2016 16:18'!
value: anObject
	^ (updateBlock value: anObject) ifTrue: [setBlock value: anObject. self changed: anObject]! !

!PluggableAdaptor class methodsFor: 'as yet unclassified' stamp: 'MM 7/22/2016 16:06'!
getBlock: aBlock setBlock: anotherBlock
	
	^ self new initialize
			getBlock: aBlock;
			setBlock: anotherBlock;
			yourself.! !

!PluggableAdaptor class methodsFor: 'as yet unclassified' stamp: 'MM 7/22/2016 16:06'!
getBlock: aBlock setBlock: anotherBlock updateBlock: updateBlock
	
	^ self new initialize
			getBlock: aBlock;
			setBlock: anotherBlock;
			updateBlock: updateBlock;
			yourself.! !

!ValueHolder methodsFor: 'as yet unclassified' stamp: 'MM 7/17/2016 16:22'!
value
	^ value! !

!ValueHolder methodsFor: 'accessing' stamp: 'MM 7/22/2016 16:18'!
value: anObject
	"Set the value of value"

	value _ anObject.
	self changed: anObject! !

!ValueHolder class methodsFor: 'as yet unclassified' stamp: 'MM 7/17/2016 16:23'!
value: anObject
	^ self new value: anObject! !
