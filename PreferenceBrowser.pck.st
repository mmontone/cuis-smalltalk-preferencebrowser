'From Cuis 6.0 [latest update: #5552] on 4 November 2022 at 11:17:16 am'!
'Description Cuis Smalltalk preferences editor.

Author: Mariano Montone (marianomontone@gmail.com)'!
!provides: 'PreferenceBrowser' 1 31!
!requires: 'Morphic-Widgets-Extras2' 1 95 nil!
!requires: 'ValueModels' 1 1 nil!
SystemOrganization addCategory: 'PreferenceBrowser'!


!classDefinition: #PreferenceEditorMorph category: 'PreferenceBrowser'!
LayoutMorph subclass: #PreferenceEditorMorph
	instanceVariableNames: 'preference model editor'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PreferenceBrowser'!
!classDefinition: 'PreferenceEditorMorph class' category: 'PreferenceBrowser'!
PreferenceEditorMorph class
	instanceVariableNames: ''!

!classDefinition: #PreferencesScrollPane category: 'PreferenceBrowser'!
PluggableScrollPane subclass: #PreferencesScrollPane
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PreferenceBrowser'!
!classDefinition: 'PreferencesScrollPane class' category: 'PreferenceBrowser'!
PreferencesScrollPane class
	instanceVariableNames: ''!

!classDefinition: #PreferenceBrowser category: 'PreferenceBrowser'!
SystemWindow subclass: #PreferenceBrowser
	instanceVariableNames: 'categoriesList selectedCategory selectedPreference preferencesPane scroller categories filter'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'PreferenceBrowser'!
!classDefinition: 'PreferenceBrowser class' category: 'PreferenceBrowser'!
PreferenceBrowser class
	instanceVariableNames: ''!


!PreferenceEditorMorph commentStamp: '<historical>' prior: 0!
The editor row for a single preference!

!PreferenceBrowser commentStamp: '<historical>' prior: 0!
Cuis Smalltalk preferences editor!

!PreferenceEditorMorph methodsFor: 'as yet unclassified' stamp: 'MM 11/4/2022 11:02:30'!
evaluatePreferenceValue
	
	| input |
	
	input := FillInTheBlankMorph 
					request: 'Preference value' 
					initialAnswer: preference value printString
					onCancel: #cancelPreferenceValue.

	(input = #cancelPreferenceValue) ifFalse: [ | val |
		val  _ Compiler evaluate: input.
		preference value: val.
		model value: val]! !

!PreferenceEditorMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/21/2016 16:43'!
handlesMouseDown: aMouseEvent
	^ true! !

!PreferenceEditorMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/21/2016 16:53'!
handlesMouseOver: aMouseEvent
	^ true! !

!PreferenceEditorMorph methodsFor: 'as yet unclassified' stamp: 'MM 11/4/2022 11:15:54'!
initialize: aPreference
	
	| preferenceName nameMorph |
	
	preference _ aPreference.
	
	self color: Color white.
	preferenceName _ self printSymbol: aPreference name.
	self setBalloonText: aPreference description.
	nameMorph _ LabelMorph contents: preferenceName.
	self
		addMorph: nameMorph
		proportionalWidth: 0.95.
	(aPreference type = Boolean) ifTrue: [
		model _ Switch new
			onAction: [ aPreference value: true ];
			offAction: [ aPreference value: false ];
			yourself.
		aPreference value
			ifTrue: [ model turnOn ]
			ifFalse: [ model turnOff ].
		editor _ ToggleButtonMorph1
			model: model
			stateGetter: #isOn
			action: #switch		]
		ifFalse: [ | f valString |
			
			valString _ preference value printString truncateWithElipsisTo: 15.
			
			model := PluggableAdaptor 
							getBlock: [preference value] 
							setBlock: [:value | preference value: value. editor label: (value printString truncateWithElipsisTo: 15)].  
			editor _ PluggableButtonMorph 
							model: self 
							action: #evaluatePreferenceValue  
							label: valString .
			editor setBalloonText: preference value printString.
			
			f _ editor fontToUse.
			editor morphExtent: ((((f widthOfString: valString) + 10) max: 3)  @ (f lineSpacing + 10)).	
		].
		model when: #changed: send: #redrawNeeded to: editor.
		self addMorph: editor! !

!PreferenceEditorMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/21/2016 18:18'!
inspectPreference
	preference inspect! !

!PreferenceEditorMorph methodsFor: 'as yet unclassified' stamp: 'MM 11/4/2022 10:58:17'!
mouseButton2Activity
	
	| menu |
	
	menu := MenuMorph new defaultTarget: self; yourself.
	
	menu addTitle: ((self printSymbol: preference name) squeezedTo: 30).
	
	"menu add: 'Set default (', (preference defaultValue printString), ')'
		    target: self
		    action: #setDefault."
		
	menu add: 'Set value'
		    target: self
		    action: #evaluatePreferenceValue.
		
	menu add: 'Inspect'
		     target: self
			action: #inspectPreference.
	
	(preference description isNil not and: [
		preference description isEmpty not]) ifTrue: [
		menu addMorphBack: (TextModelMorph withText: preference description)].
		
	menu popUpInWorld: self world.
		
	
	
	
	! !

!PreferenceEditorMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/21/2016 17:10'!
mouseEnter: aMouseEvent
	self color: (Color r: 0.942 g: 0.942 b: 0.942).! !

!PreferenceEditorMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/21/2016 17:08'!
mouseLeave: aMouseEvent
	self color: Color white.! !

!PreferenceEditorMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/21/2016 16:25'!
printSymbol: aSymbol
	| stream printed |
	stream _ aSymbol string readStream.
	printed _ WriteStream on: ''.
	printed nextPut: stream next asUppercase.
	[ stream atEnd ] whileFalse: [ | char |
		char _ stream next.
		char isUppercase ifTrue: [
			printed nextPut: Character space.
			char _ char asLowercase ].
		printed nextPut: char ].
	^ printed contents.! !

!PreferenceEditorMorph methodsFor: 'as yet unclassified' stamp: 'MM 7/22/2016 16:14'!
setDefault
	"model value: preference defaultValue"
	
	preference preferenceValue: preference defaultValue.
	
	model value: preference defaultValue.! !

!PreferenceEditorMorph class methodsFor: 'as yet unclassified' stamp: 'MM 7/21/2016 16:23'!
on: aPreference
	^ self newRow initialize: aPreference.! !

!PreferencesScrollPane methodsFor: 'as yet unclassified' stamp: 'MM 7/21/2016 16:38'!
getMenu
	^ nil! !

!PreferenceBrowser methodsFor: 'GUI building' stamp: 'MM 7/21/2016 16:23'!
buildPreferenceEditorFor: aPreference
	^ PreferenceEditorMorph on: aPreference! !

!PreferenceBrowser methodsFor: 'GUI building' stamp: 'MM 4/25/2022 13:35:51'!
buildPreferenceEditorsPane
	preferencesPane _ LayoutMorph newColumn.
	"preferencesPane color: Color white."
	self buildPreferencesPane.
	scroller _ PreferencesScrollPane new.
	scroller scroller: preferencesPane.
	scroller color: Color white.
	scroller scroller color: Color white.
	scroller setScrollDeltas.
	^ scroller.! !

!PreferenceBrowser methodsFor: 'GUI building' stamp: 'MM 12/23/2020 15:47:04'!
buildPreferencesPane
	preferencesPane axisEdgeWeight: 0.
	self selectedCategoryPreferences do: [ :aPreference |
		preferencesPane
			addMorph: (self buildPreferenceEditorFor: aPreference)
			fixedHeight: 20 ].
	preferencesPane morphExtent: 400 @ (self selectedCategoryPreferences size * 20).
	
	^ preferencesPane.! !

!PreferenceBrowser methodsFor: 'GUI building' stamp: 'MM 11/1/2018 12:13'!
updatePreferencesList

	preferencesPane removeAllMorphs.
	self buildPreferencesPane.
	scroller scroller morphHeight: (self selectedCategoryPreferences size * 20).
	scroller setScrollDeltas.! !

!PreferenceBrowser methodsFor: 'as yet unclassified' stamp: 'MM 11/1/2018 11:32'!
categories
	^ #(#'-- all --') , (categories ifNil: [categories := Preferences categories. categories])! !

!PreferenceBrowser methodsFor: 'as yet unclassified' stamp: 'MM 7/21/2016 17:51'!
initialExtent
	^  552.0@389.0! !

!PreferenceBrowser methodsFor: 'as yet unclassified' stamp: 'MM 7/21/2016 17:47'!
minimumExtent
	^  546.0@163.0! !

!PreferenceBrowser methodsFor: 'as yet unclassified' stamp: 'MM 11/4/2022 10:52:40'!
preferences
	^ Preferences allPreferences! !

!PreferenceBrowser methodsFor: 'as yet unclassified' stamp: 'MM 11/4/2022 10:56:15'!
preferencesInCategory: aCategory
	
	^ Preferences allPreferences select: [:aPreference | aPreference category = aCategory]! !

!PreferenceBrowser methodsFor: 'as yet unclassified' stamp: 'MM 12/23/2020 16:13:00'!
selectedCategoryPreferences

	|preferences|
	
	preferences _ (self selectedCategory == #'-- all --') 
		ifTrue: [self preferences]	
		ifFalse: [self preferencesInCategory: self selectedCategory].
		
	filter ifNotEmpty: [
		preferences _ preferences select: [:p | p name asString includesSubstring: filter asString caseSensitive: false]].
	
	^ preferences! !

!PreferenceBrowser methodsFor: 'accessing' stamp: 'MM 7/21/2016 12:16'!
categoriesList
	"Answer the value of categoriesList"

	^ categoriesList! !

!PreferenceBrowser methodsFor: 'accessing' stamp: 'MM 7/21/2016 12:16'!
categoriesList: anObject
	"Set the value of categoriesList"

	categoriesList _ anObject! !

!PreferenceBrowser methodsFor: 'accessing' stamp: 'MM 11/1/2018 11:51'!
filter

	^ filter! !

!PreferenceBrowser methodsFor: 'accessing' stamp: 'MM 11/1/2018 12:17'!
filter: aText

	filter _ aText.
	
	self changed: #clearUserEdits.
	self updatePreferencesList ! !

!PreferenceBrowser methodsFor: 'accessing' stamp: 'MM 7/21/2016 12:16'!
selectedCategory
	"Answer the value of selectedCategory"

	^ selectedCategory! !

!PreferenceBrowser methodsFor: 'accessing' stamp: 'MM 11/1/2018 12:13'!
selectedCategory: anObject
	"Set the value of selectedCategory"
	selectedCategory _ anObject.
	
	self updatePreferencesList.! !

!PreferenceBrowser methodsFor: 'accessing' stamp: 'MM 7/21/2016 12:16'!
selectedPreference
	"Answer the value of selectedPreference"

	^ selectedPreference! !

!PreferenceBrowser methodsFor: 'accessing' stamp: 'MM 7/21/2016 12:16'!
selectedPreference: anObject
	"Set the value of selectedPreference"

	selectedPreference _ anObject! !

!PreferenceBrowser methodsFor: 'initialization' stamp: 'MM 11/4/2022 11:09:32'!
initialize
	
	| row leftPane filterInput |
	
	super initialize.
	self setLabel: 'Preferences'.
	selectedCategory _ self categories first.
	filter _ ''.
	
	row _ LayoutMorph newRow.
	
	leftPane := LayoutMorph newColumn.
	leftPane axisEdgeWeight: 0.
	
	categoriesList _ PluggableListMorphByItem
		model: self
		listGetter: #categories
		indexGetter: #selectedCategory
		indexSetter: #selectedCategory:.
		
	leftPane addMorphUseAll: categoriesList.
	
	filterInput _ 	(TextModelMorph
						textProvider: self
						textGetter: #filter
						textSetter: #filter:) 
						acceptOnCR: true;
						hideScrollBarsIndefinitely;
						borderWidth: 1;
						setBalloonText: 'Preferences filter';
						yourself.
	leftPane addMorph: filterInput layoutSpec: (LayoutSpec proportionalWidth: 1 fixedHeight: 15).
	
	row
		addMorph: leftPane
		proportionalWidth: 0.3.
	row addMorphUseAll: self buildPreferenceEditorsPane.
	self addMorph: row.! !

!PreferenceBrowser class methodsFor: 'as yet unclassified' stamp: 'MM 7/23/2016 13:04'!
open
	^ self new openInWorld! !

!Switch methodsFor: '*PreferenceBrowser' stamp: 'MM 7/22/2016 15:54'!
value
	^ on! !

!Switch methodsFor: '*PreferenceBrowser' stamp: 'MM 7/22/2016 15:54'!
value: value
	value ifTrue: [self turnOn] ifFalse: [self turnOff]
	! !
