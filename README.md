# Preference Browser #

This is a Preference Browser for Cuis Smalltalk

![Screenshot](https://bitbucket.org/mmontone/cuis-smalltalk-preferencebrowser/downloads/PreferenceBrowser.png "Screenshot")

## Install ##

```
Feature require: 'PreferenceBrowser'
``` 
## Run ##

From World Menu (right click on desktop):
    Preferences -> All preferences ...

Programatically:

     PreferenceBrowser open.